import requests


def get_gold_price_today() -> float:
    date = '2019-04-15'
    response = requests.get(f'http://api.nbp.pl/api/cenyzlota/ {date} /?format=json')
    return response.json()[0]['cena']


def show_how_much_gold(my_money: float, gold_price: float) -> float:
    return my_money / gold_price


def get_specific_currency(currency: str, date: str = 'today') -> dict:
    response = requests.get(f'http://api.nbp.pl/api/exchangerates/rates/c/{currency}/{date}/?format=json')

    currencies = {}

    currencies['currency_name'] = response.json()['currency']
    currencies['currency_code'] = response.json()['code']
    currencies['currency_bid'] = response.json()['rates'][0]['bid']
    currencies['currency_ask'] = response.json()['rates'][0]['ask']
    return currencies


def get_ask_price(currency: str, date: str = 'today') -> float:
    response = requests.get(f'http://api.nbp.pl/api/exchangerates/rates/c/{currency}/{date}/?format=json')
    return response.json()['rates'][0]['ask']


def get_bid_price(currency: str, date: str = 'today') -> float:
    response = requests.get(f'http://api.nbp.pl/api/exchangerates/rates/c/{currency}/{date}/?format=json')
    return response.json()['rates'][0]['bid']


def convert_currency_ask_not_PLN(currency1: str, currency2: str, my_money: float, date: str = 'today') -> float:
    ask1 = get_ask_price(currency1, date)  # input currency in PLN
    ask2 = get_ask_price(currency2, date)  # output currency course
    return my_money * ask1 / ask2  # PLN to input curr to output curr


def convert_currency_bid_not_PLN(currency1: str, currency2: str, my_money: float, date: str = 'today') -> float:
    bid1 = get_bid_price(currency1, date)
    bid2 = get_bid_price(currency2, date)
    return my_money * bid1 / bid2


def convert_currency_bid_from_PLN(currency: str, my_money: float, date: str = 'today') -> float:
    return get_bid_price(currency, date)


def convert_currency_ask_from_PLN(currency: str, my_money: float, date: str = 'today') -> float:
    return get_ask_price(currency, date)

# if __name__ == "__main__":
#     print(get_specific_currency('usd', '2021-04-15'))