from django.urls import path
from money_changer.views import test_response

urlpatterns = [
    path('test/', test_response)
]
